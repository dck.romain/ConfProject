var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
  },
  username: {
    type: String,
  },
  password: {
    type: String,
  }
});

var User = mongoose.model('users', UserSchema);
module.exports = { 
  User:User 
}