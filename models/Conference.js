'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConferenceSchema = new Schema({
  name: {
    type: String
  },
  status: {
    type: String
  },
  date_start: {
    type: String
  },
  date_end: {
    type: String
  },
  nbInsc: {
    type: Number
  }
});

var Conference = mongoose.model('conferences', ConferenceSchema);
module.exports = { 
  Conference:Conference
}