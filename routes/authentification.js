var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
const User = require('../models/User').User;
const jwt = require('jsonwebtoken');
const localStorage = require('localStorage');

/* GET users listing. */
router.get('/', function(req, res, next) {
    if (localStorage.getItem('user') != null) {
        localStorage.removeItem('user');
        var action = "Connexion"
        res.render('index', { action });
    }
  res.render('authentification');
});

router.post('/', function(req, res, next) {
    //authenticate input against database
  User.findOne({ email: req.body.email }, function (err, user) {
      console.log(user);
      if (user != null) {
          const jwtKey = process.env.JWT_KEY || 'secret';
          const token = jwt.sign({email: user.email, id: user.id}, jwtKey, {expiresIn:'1h'});
          req.headers.authorization = token;
          localStorage.setItem('user', user._id);
          res.redirect('./');
      } else {
          res.render('authentification');
      }
    });
});

module.exports = router;