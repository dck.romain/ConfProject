const checkAuth = require('./check-auth');
var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
const Conference = require('../models/Conference').Conference;
const localStorage = require('localStorage');

router.get('/', (req, res) => {

  if (localStorage.getItem('user') != null) {
    Conference.find({}, function(err, conference) {
      if (err)
        res.send(err);
      res.render('conferences', { conference });
    });
  } else {
    var action="Connexion";
    res.redirect('/', { action });
  }
})

router.get('/addConf', (req, res) => {
  console.log("test");
  res.render('./addConf');
})

router.get('/:_id', function(req, res, next) {
  console.log("test2");
  Conference.findOne({ _id: req.params._id }, function (err, conference) {
    conference.nbInsc++;
    conference.save((err, conference) => {
    });
    res.redirect('/conferences');
  });
});

router.post('/addConf', function(req, res, next) {
  
    if (req.body.name &&
      req.body.status &&
      req.body.date_start &&
      req.body.date_end) {
    
      const confData =  new Conference ({
        name: req.body.name,
        status: req.body.status,
        date_start: req.body.date_start,
        date_end: req.body.date_end,
        nbInsc: 0
      });
  
      confData.save((err, confData) => {
      });
  
      return res.redirect('./');
    } else {
        return res.redirect('/');
    }
  });

module.exports = router;