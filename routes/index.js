var express = require('express');
var router = express.Router();
const localStorage = require('localStorage');

/* GET home page. */
router.get('/', function(req, res, next) {

  var action;

  if (localStorage.getItem('user') == null) {
    action = "Connexion";
  } else {
    action = "Deconnexion";
  }

  res.render('index', { action});
});

module.exports = router;
