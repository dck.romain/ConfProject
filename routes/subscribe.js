var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
const User = require('../models/User').User;

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('subscribe');
});

router.post('/', function(req, res, next) {
  
  if (req.body.email &&
    req.body.username &&
    req.body.password) {
  
    const userData =  new User ({
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    });

    userData.save((err, userData) => {
    });

    return res.redirect('./authentification');
  } else {
      return res.redirect('/');
  }
});

module.exports = router;